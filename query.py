def illegal():
    print("Invalid Input")
    exit()

def cluster_ch():
    print("请选择问题类型(输入A,B,C...)")
    type=input("A.deepin简介  B.linux相关问题  C.技术规范与wiki编辑  D.系统修复/美化/其他知识点  E.硬件问题  F.软件问题  G.玲珑相关问题  H.其他\n")
    if(type=='A'):
        type2=input("请选择问题类型A：deepin历史、团队及社区  B：deepin技术问题\n")
        if(type2=='A'):
            path="A-A.json"
        elif(type2=='B'):
            path="A-B.json"
        else:
            illegal()
    elif(type=='B'):
        path="B.json"
    elif(type=='C'):
        path="C.json"
    elif(type=='D'):
        path="D.json"
    elif(type=='E'):
        path="E.json"
    elif(type=='F'):
        type2=input("请选择软件类型A：GUI软件  B：服务环境搭建  C：开发者软件  D：命令行软件  E：普通用户软件  F：游戏及其他特定领域软件\n")
        if(type2=='A'):
            type3=input("请选择软件开发者A：deepin开发  B：第三方开发\n")
            if(type3=='A'):
                path="F-A-A.json"
            if(type3=='B'):
                path="F-A-B.json"
            else:
                illegal()
        elif(type2=='B'):
            path="F-B.json"
        elif(type2=='C'):
            path="F-C.json"
        elif(type2=='D'):
            path="F-D.json"
        elif(type2=='E'):
            path="F-E.json"
        elif(type2=='F'):
            path="F-F.json"
        else:
            illegal()
    elif(type=='G'):
        path="G.json"
    elif(type=='H'):
        path="H.json"
    else:
        illegal()
    return path

def cluster_en():
    print("Please select your question's type(input A,B,C...)")
    type=input("A.Introduction of deepin  B.About linux  C.Technical Specifications and Wiki Editing  D.System Repair, Beautification and Other Knowledge  E.About Hardware  F.About Software  G.Questions About Linglong  H.Others\n")
    if(type=='A'):
        type2=input("Please select question's type A：deepin's history, team and community  B：deepin's technology\n")
        if(type2=='A'):
            path="A-A.json"
        elif(type2=='B'):
            path="A-B.json"
        else:
            illegal()
    elif(type=='B'):
        path="B.json"
    elif(type=='C'):
        path="C.json"
    elif(type=='D'):
        path="D.json"
    elif(type=='E'):
        path="E.json"
    elif(type=='F'):
        type2=input("Please choose software's type A：GUI  B：Service environment construction  C：For Developers  D：CMD  E：Ordinary user  F：Games and other specific softwares\n")
        if(type2=='A'):
            type3=input("Please choose software's developer A：deepin  B：Third Party\n")
            if(type3=='A'):
                path="F-A-A.json"
            if(type3=='B'):
                path="F-A-B.json"
            else:
                illegal()
        elif(type2=='B'):
            path="F-B.json"
        elif(type2=='C'):
            path="F-C.json"
        elif(type2=='D'):
            path="F-D.json"
        elif(type2=='E'):
            path="F-E.json"
        elif(type2=='F'):
            path="F-F.json"
        else:
            illegal()
    elif(type=='G'):
        path="G.json"
    elif(type=='H'):
        path="H.json"
    else:
        illegal()
    return path

def input_query(base_path, language):
    if language=='zh':
        path = base_path+cluster_ch()
        query= input("请输入问题：")
    elif language=='en':
        path = base_path+cluster_en()
        query= input("Please input your question: ")
    return path, query