import openai
import json

with open(".\config.json", "r", encoding="utf-8") as f:
    config = json.load(f)
API=config['GPT-API']

def GPT(prompt, type):
    openai.api_key = API
    prompt=prompt[0:16000]
    if type=="QA":
        sys_msg="你是一个自动问答系统；请按照要求回答问题。"
    elif type=="Translate":
        sys_msg="你是一个翻译机器人"
    completion = openai.ChatCompletion.create(
    model="gpt-3.5-turbo-16k",
    messages=[
        {"role": "system", "content": sys_msg},
        {"role": "user", "content": prompt}],
    temperature=0.2)
    return completion.choices[0].message.content