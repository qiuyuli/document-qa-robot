### Project-225: document-question-answering-bot（文档问答机器人）

队伍名称：ArkShin

团队成员：邱昱力、钱星雨、何婉欣

简介：这是大学生OS赛的一道赛题，旨在实现一个针对deepin操作系统完成问答的QA专家系统。我们使用sBERT+LLM构造了一个检索增强的专家系统。

#### 相关链接：

赛题链接：[GitHub - oscomp/proj225-document-question-answering-bot](https://github.com/oscomp/proj225-document-question-answering-bot)

演示文档：仓库目录下 demo.pdf

项目文档：仓库目录下 项目文档.pdf

博客链接：[(62条消息) document-question-answering-bot（文档问答机器人）_。未央的博客-CSDN博客](https://blog.csdn.net/weixin_61255639/article/details/131024787?spm=1001.2014.3001.5502)

#### 项目部署：

按仓库的目录结构下载至本地，首先配置config.json文件。我们的问答机器人支持cmd命令行访问和web访问。

需确保电脑已配置Python3的环境，并命令行运行以下指令安装必需的第三方库：

```
pip install scipy
pip install openai
pip install sentence_transformers
```

当web访问时，首先进入项目所在的目录，运行server.py启动服务器；在浏览器访问127.0.0.1:8000端口即可。

当cmd访问时，首先进入项目所在的目录，直接运行main.py即可。

当使用ErnieBot或Spark作为基座模型时，需要确保计算机接入互联网；若使用gpt-3.5作为基座模型，还应保证计算机能够访问openAI相关外网网站。

【使用LLM前请自己到官网获取对应模型的API密钥】

#### 仓库目录和文件描述：

demo.pdf：						机器人问答效果展示

data.zip：   						存放了deepin wiki网站内容的所有文档，用于训练。

ch-BERT：   						预训练后的中文BERT模型

embedding:                  存放文档嵌入后的向量表示

GPT_turbo.py：     			调用gpt-3.5接口实现问答系统输出

ErnieBot_turbo.py：    	调用ErnieBot接口实现问答系统输出

SparkApi.py：              	 调用星火Spark接口实现问答系统输出

LLM.py：                         	集成了GPT、ErnieBot、Spark等多种语言模型的接口，设置prompt

main.py：  			 			cmd命令行访问时的程序入口

main_thread.py：        	web访问时的程序入口，由server调用以进入模型

sBERT.py： 						实现sBERT架构，完成文档匹配

query.py： 						cmd访问时实现问题的输入

doc_embedding.py：  	对文档进行嵌入，存储得到的向量信息

config.json：				 	配置文件

dataset.json：			   	问答测试集，包含30条基于deepin文档的问答。

index.html：                  	前端页面文档

style.css：                       	前端页面文档

script.js：                        	前端页面文档

send.png/bot.png/user.png:  前端页面插图

demo.pdf：						演示文档

项目文档.pdf：				   项目介绍文档

#### 开发日程表：

2023年4月27日——完成对项目技术路线的调研，确定小组分工；开始试验。

2023年5月10日——从[https://wiki.deepin.org](https://wiki.deepin.org/) 上获取了全部信息并存储在本地。

2023年5月15日——获取了GPT的API并就API回答部分进行构件测试。

2023年5月19日——初步完成了sBERT模型的文档匹配，效果不佳。

2023年5月23日——与导师交流项目进展，并就问题提出建议。

2023年5月25日——经过试验使用Chinese-BERT-wwm进行词嵌入，提升了匹配效果；就该部分构件测试。

2023年6月2日——完成数据集构造；完成系统各部分的组成并进行系统测试。

2023年7月12日——设置文本向量数据文件，以加快文档匹配速度。

2023年7月16日——重新设计prompt，为机器人添加多样化的回答能力。

2023年7月25日——添加了对英语的支持。

2023年8月2日——为LLM添加了ErnieBot的选项，并计算测试效果。

2023年8月8日——为LLM添加了对玲珑问答的支持。

2023年8月12日——为LLM添加了Spark的选项，计算ROUGE-L分数。

2023年8月14日——完成web页面及和后端的连通调试。

