from sentence_transformers import SentenceTransformer as ST
import json
from sBERT import multi_ans
from ErnieBot_turbo import ErnieBot
from GPT_turbo import GPT
from SparkApi import Spark

def llm_model(name):
    if name=='ErnieBot':
        return ErnieBot
    elif name=='gpt':
        return GPT
    elif name=='Spark':
        return Spark

with open(".\config.json", "r", encoding="utf-8") as f:
    config = json.load(f)

def nlp_gen(llm, language, json_path, query):
    model_embed = ST(config['model_embed'])
    data_path   = config['data_path']
    model_llm   = llm_model(llm)
    config_path = config['embedding_path']
    json_path   = config_path+json_path
    with open(json_path,'r',encoding="utf-8") as f:
        dict = json.load(f)
    file_name, sentence_embedding, url=  [], [], []
    for i in dict:
        url.append(dict[i][0])
        file_name.append(i)
        sentence_embedding.append(dict[i][1])
    ans=multi_ans(query, url, file_name, sentence_embedding, model_embed, model_llm, data_path, language)
    return ans