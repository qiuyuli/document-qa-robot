from sentence_transformers import SentenceTransformer as ST
import os
import json

with open(".\config.json", "r", encoding="utf-8") as f:
    config = json.load(f)
model = ST(config['model'])
path="docs_path"
dict={}

#embedding
files=os.listdir(path)
for file_name in files:
    with open(os.path.join(path,file_name),"r",encoding="UTF-8") as f:
        print(file_name)
        url=(f.readlines()[0])   #网址
        sentence=f.read()        #docs
        sentence_embedding=model.encode(sentence).tolist()
    dict.update({file_name : [url, sentence_embedding]})
#convert to json
json_str = json.dumps(dict,indent=4)
with open('json_filepath', 'w') as json_file:
    json_file.write(json_str)