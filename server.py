from http.server import BaseHTTPRequestHandler, HTTPServer
import json
from main_thread import nlp_gen

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def _set_response(self, status_code=200, content_type='text/html'):
        self.send_response(status_code)
        self.send_header('Content-type', content_type)
        self.end_headers()

    def do_GET(self):
        if self.path == '/':
            self.path = '/index.html'
        elif self.path.endswith('.css'):
            self._set_response(200, 'text/css')
            with open(self.path[1:], 'rb') as css_file:
                self.wfile.write(css_file.read())
            return
        elif self.path.endswith('.png'):  # Handle .png image requests
            self._set_response(200, 'image/png')
            with open(self.path[1:], 'rb') as image_file:
                self.wfile.write(image_file.read())
            return
        try:
            file_to_open = open(self.path[1:],'r',encoding='utf-8').read()
            self.send_response(200)
        except:
            file_to_open = "File not found"
            self.send_response(404)
        self._set_response()
        self.wfile.write(file_to_open.encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode('utf-8')
        data = json.loads(post_data)
        try:
            model_llm = data['llm']
            language  = data['language']
            query     = data['query']
            category  = data['category']
            subcategory = data['subcategory']
            if subcategory=="":
                json_path=category
            elif subcategory=='deepin历史、团队及社区':
                json_path="A-A.json"
            elif subcategory=='deepin技术问题':
                json_path="A-B.json"
            elif subcategory=='GUI软件之deepin开发':
                json_path="F-A-A.json"
            elif subcategory=='GUI软件之第三方开发':
                json_path="F-A-B.json"
            elif subcategory=='服务环境搭建':
                json_path="F-B.json"
            elif subcategory=='开发者软件':
                json_path="F-C.json"
            elif subcategory=='命令行软件':
                json_path="F-D.json"
            elif subcategory=='普通用户软件':
                json_path="F-E.json"
            elif subcategory=='游戏及其他特定领域软件':
                json_path="F-F.json"
            print(model_llm, language, json_path, query)
            answer   = nlp_gen(model_llm, language, json_path, query)
            response = {'answer': answer}
            self._set_response(200, 'application/json')
            self.wfile.write(json.dumps(response).encode('utf-8'))
        except:
            self._set_response(400, 'application/json')
            self.wfile.write(json.dumps({'error': 'Invalid input'}).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=SimpleHTTPRequestHandler, port=8000):
    server_address = ('127.0.0.1', port)
    httpd = server_class(server_address, handler_class)
    print(f"Starting server on port {port}...")
    httpd.serve_forever()

if __name__ == '__main__':
    run()