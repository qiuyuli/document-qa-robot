from GPT_turbo import GPT
class LLM:
    def __init__(self, model):
        self.model=model
    def get_QA(self, question, ref, language, en=""):
        if language=="en":
            en="Please answer the question in English"
        if self.model==GPT:
            prompt=en+f'''请根据<>中参考文档的内容回答问题，回答的语言要和提问的语言一致。你的回答中不必包括参考链接。\
            请先从下面的3种情况中选择符合的情况，按要求输出：
            当能确定答案时，直接输出答案;\
            当感到问题描述不够清晰时发起追问，输出格式是"追问：你的追问内容"；\
            当文档中没有匹配的内容时，输出"对不起，我没有从下面的文档中找到相关内容"；\
            问题：<{question}>    参考文档：<{ref}>'''
        else:
            prompt=en+f'''你是一个问答机器人，请根据<>中参考文档的内容回答问题；你的回答不用包括参考链接"；\
            如果你觉得文档中没有匹配的内容，直接输出"对不起，我没有从下面的文档中找到相关内容"；
            问题：<{question}>    参考文档：<{ref}>'''
        ans=self.model(prompt, "QA")
        return ans
    def translation(self, query):
        prompt=f'''你的任务是把<>中的语言翻译为中文。文本：<{query}>'''
        ans=self.model(prompt, "Translate")
        return ans