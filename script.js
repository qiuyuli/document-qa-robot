// script.js
const userAvatar = new Image();
userAvatar.src = "bot.png";

window.onload = function() {
    updateSubCategory();
    updateTopBar();

    // 修改模型选择器的事件处理程序
    document.getElementById('model-selector').addEventListener('change', function() {
        updateTopBar();
    });

};

// 显示conversation框
document.getElementById('conversation').style.display = 'block';
appendMessage('bot', '你好，这里是deepin助手！\
    \n你可以询问我任何关于deepin系统的问题；提问前请先选择问答模型和问题类型!');

function appendMessage(who, text) {
    const conversation = document.getElementById('conversation');
    const message = document.createElement('div');
    message.className = 'message ' + who;

    // 创建气泡元素
    const bubble = document.createElement('div');
    bubble.className = 'bubble';
    bubble.textContent = text;
    bubble.style.whiteSpace = 'pre-line'; // 允许换行

    // 创建头像元素
    const avatar = document.createElement('div');
    avatar.className = 'avatar';

    // 如果是用户，先添加气泡，再添加头像
    if (who === 'user') {
        message.appendChild(bubble);
        message.appendChild(avatar);
    } else {
        // 如果是机器人，先添加头像，再添加气泡
        message.appendChild(avatar);
        message.appendChild(bubble);
    }

    conversation.appendChild(message);
    conversation.scrollTop = conversation.scrollHeight;
}


// 添加函数来更新顶部栏的文本
function updateTopBar() {
    const model = document.getElementById('model-selector').value;
    const modelName = {
        'gpt': 'GPT-3.5',
        'ErnieBot': '文心一言',
        'Spark': 'Spark'
    };
    document.getElementById('top-bar').textContent = '当前模型：' + modelName[model];
}

function updateSubCategory() {
    const category = document.getElementById('category-selector').value;
    //new
    const subcategoryContainer = document.getElementById('subcategory-select-box');
    const subcategorySelector = document.getElementById('subcategory-selector');
    // 清空现有选项
    subcategorySelector.innerHTML = '';
    let subcategories = [];
    if (category === 'A.json') {
        subcategories = ['deepin历史、团队及社区', 'deepin技术问题'];
    } else if (category === 'F.json') {
        subcategories = ['GUI软件之deepin开发','GUI软件之第三方开发', '服务环境搭建', '开发者软件', '命令行软件', '普通用户软件', '游戏及其他特定领域软件'];
    }
    
    //new
    // 如果有子类别，显示子类别选择框，并添加子类别选项
    if (subcategories.length > 0) {
        document.getElementById('subcategory-select-box').disabled = false;
        document.getElementById('subcategory-selector').disabled = false;
        subcategoryContainer.style.display = 'block';
        for (const subcategory of subcategories) {
            const option = document.createElement('option');
            option.value = subcategory;
            option.textContent = subcategory;
            subcategorySelector.appendChild(option);
        }
    } else {
        // 如果没有子类别，隐藏子类别选择框
        document.getElementById('subcategory-select-box').disabled = true;
        document.getElementById('subcategory-selector').disabled = true;
        //subcategoryContainer.style.display = 'none';
    }
}

function submitQuestion() {
    const llm = document.getElementById('model-selector').value;
    const language = document.getElementById('language-selector').value;
    const category = document.getElementById('category-selector').value;
    const subcategory = document.getElementById('subcategory-selector').value; 
    const query = document.getElementById('user-input').value;    


    appendMessage('user', query);
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://127.0.0.1:8000/", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) 
    {
        if (xhr.status === 200) 
        {
            const result = JSON.parse(xhr.responseText);
            // 使用机器人回答调用appendMessage函数
            appendMessage('bot', result.answer);
            console.log(result)
        }
    }
    };
    const data = JSON.stringify({llm, language, category, subcategory, query});

    // 清空输入框
    document.getElementById('user-input').value = '';

    xhr.send(data);
}